import React, { useState } from 'react';

function PetForm({ onAddPet }) {
  const [pet, setPet] = useState({ name: '', species: '', age: '' });

  const handleInputChange = (e) => {
    setPet({ ...pet, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    onAddPet(pet);
    setPet({ name: '', species: '', age: '' });
  };

  return (
    <form onSubmit={handleSubmit}>
      <h2>Add a New Pet</h2>
      <label>
        Name:
        <input type="text" name="name" value={pet.name} onChange={handleInputChange} />
      </label>
      <label>
        Species:
        <input type="text" name="species" value={pet.species} onChange={handleInputChange} />
      </label>
      <label>
        Age:
        <input type="text" name="age" value={pet.age} onChange={handleInputChange} />
      </label>
      <button type="submit" className="add-pet-button">Add Pet</button>
    </form>
  );
}

export default PetForm;
