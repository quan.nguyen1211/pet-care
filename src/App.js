import React, { useState } from 'react';
import './App.css';
import PetForm from './PetForm';

function App() {
  const [pets, setPets] = useState([
    {
      name: 'Fluffy',
      species: 'Cat',
      age: 3,
      image: '/images/https://upload.wikimedia.org/wikipedia/commons/a/a5/Red_Kitten_01.jpg', // Đường dẫn hình ảnh của mèo
    },
    {
      name: 'Buddy',
      species: 'Dog',
      age: 2,
      image: '', // Đường dẫn hình ảnh của chó
    },
    {
      name: 'Hoppy',
      species: 'Rabbit',
      age: 1,
      image: '/images/rabbit.jpg', // Đường dẫn hình ảnh của thỏ
    },
  ]);
  
  const [currentPet, setCurrentPet] = useState(null);

  const addPet = (pet) => {
    setPets([...pets, pet]);
  };
  

  return (
    <div className="App">
      <h1>Pet Care App</h1>
      <div>
        <h2>Pet List</h2>
        <ul>
          {pets.map((pet, index) => (
            <li key={index}>
              <button onClick={() => setCurrentPet(pet)}>{pet.name}</button>
            </li>
          ))}
        </ul>
      </div>

      <PetForm onAddPet={addPet} />

      <div>
        {currentPet ? (
          <div>
            <h2>Pet Details</h2>
            <p>Name: {currentPet.name}</p>
            <p>Species: {currentPet.species}</p>
            <p>Age: {currentPet.age}</p>
          </div>
        ) : (
          <p>Select a pet to view details</p>
        )}
      </div>
    </div>
  );
}

export default App;
